#ifndef GLPKL1MINIMISATION_H
#define GLPKL1MINIMISATION_H
#include <string>
#include <vector>
#include <glpk.h>
#include <cassert>

#ifndef GLPKFORMAT_H
#include "GLPKFormat.h"
#endif

#ifndef UTILITIES_H
#include "utilities.h"
#endif

#ifndef L1MINIMISATION_H
#include "L1Minimisation.h"
#endif

using namespace std;


// ---------------------------------------
// ----- GLPK specialisation header
// ---------------------------------------


class GLPKL1Minimisation: public L1Minimisation {
protected:
	// size parameters
	// unsigned _nvertices; 
	unsigned _dim;
	

	// GLPK variables
	glp_prob *_lp = nullptr;
	glp_smcp _parm;
	int _glp_ret = 0;
	string _method = "simplex";

	// optional vector of strings that identify the vertices 
	vector<string> _labels;

	// output
	unsigned _verbose = 2;


	// this updates the GLPK problem using the GLPKFormat struct data
	void update_problem(GLPKFormat& data);

public:
	GLPKL1Minimisation();
	GLPKL1Minimisation(string cmatrix_file);
	GLPKL1Minimisation(vector<vector<int>>& cmatrix);
	GLPKL1Minimisation(vector<LabelledState>& cmatrix);

	// Destructor
	~GLPKL1Minimisation();

	// input
	int read_vertex_matrix(string cmatrix_file);
	int read_vertex_matrix(vector<vector<int>>& cmatrix);
	int read_vertex_matrix(vector<LabelledState>& cmatrix);

	// operations
	int check_point(vector<double> &y);

	// get methods
	double get_obj_value() ;
	int get_status();
	unsigned get_nvertices();
	unsigned get_dimension();
	unsigned get_nnz();

	// set methods
	void set_verbosity(unsigned level);
	void set_method(string s);

	// output methods
	void write_glpk_output(string outfile);
	void write_sol(string outfile);
	void print_parameters();
	void write_constraint_matrix(string outfile);
};


#endif