#ifndef GLPKCONVEXSEPERATION_H
#define GLPKCONVEXSEPERATION_H
#include <glpk.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <stdexcept>
#include <algorithm>

#ifndef UTILITIES_H
#include "utilities.h"
#endif

#ifndef GLPKFORMAT_H
#include "GLPKFormat.h"
#endif

#ifndef CONVEXSEPERATION_H
#include "ConvexSeparation.h"
#endif

#ifndef POINTGENERATOR_H
#include "PointGenerator.h"
#endif

using namespace std;


// ---------------------------------------
// ----- GLPK specialisation header
// ---------------------------------------

class GLPKConvexSeparation: public ConvexSeparation {
protected:
	// size parameters
	// unsigned _nvertices; 
	unsigned _dim;
	
	// coordinates of a point to check
	vector<double> _y;

	// GLPK variables
	glp_prob *_lp = nullptr;
	glp_smcp _parm;
	int _glp_ret = 0;
	vector<int> _ind;
	string _method = "simplex";

	// interval division parameters
	double _lbnd = 0;
	double _ubnd = 1;
	unsigned _max_iter = 50;
	double _precision = 1e-6;

	// optional vector of strings that identify the vertices 
	vector<string> _labels;

	// output
	unsigned _verbose = 2;

	// this updates the GLPK problem using the GLPKFormat struct data
	void update_problem(GLPKFormat& data);

public:
	GLPKConvexSeparation();
	GLPKConvexSeparation(const unsigned dimension);
	GLPKConvexSeparation(const string cmatrix_file);
	GLPKConvexSeparation(const vector<vector<int>>& cmatrix);
	GLPKConvexSeparation(const vector<LabelledState>& cmatrix);

	// Copy constructor
	GLPKConvexSeparation(const GLPKConvexSeparation& other);

	// Copy assignment via copy-and-swap
	GLPKConvexSeparation& operator=(GLPKConvexSeparation rhs);

	// Move constructor
	GLPKConvexSeparation(GLPKConvexSeparation&& other);

	// Move assignment
	GLPKConvexSeparation& operator=(GLPKConvexSeparation&& rhs);

	// Destructor
	~GLPKConvexSeparation();

	// input
	int read_vertex_matrix(const string cmatrix_file);
	int read_vertex_matrix(const vector<vector<int>>& cmatrix);
	int read_vertex_matrix(const vector<LabelledState>& cmatrix);

	// operations
	int check_point(const vector<double> &y);
	int check_point(const vector<int> &y);
	double check_family(const PointGenerator &y);
	void delete_point(const unsigned number);
	int delete_redundant_points(const unsigned max_loops=1);
	int add_vertex(const vector<double> &v, const string label="");
	int add_vertex(const vector<int> &v, const string label="");
	int add_vertex(const LabelledState& v);

	// performs Dula-Helgason algorithm to delete redundant points
	// points have to be inputted in lexicographical order for this to succeed! duplicate points are not allowed!
	// returns the indices of points that are extremal
	vector<unsigned> add_vertices(const vector<LabelledState>& points);
	void set_labels(const vector<string>& labels);

	// get methods
	double get_obj_value();
	vector<double> get_solution();
	vector<double> get_vertex(const unsigned i);
	vector<int> iget_vertex(const unsigned i);
	int get_status();
	unsigned get_nvertices();
	unsigned get_dimension();
	unsigned get_nnz();
	vector<string> get_labels();
	vector<vector<double>> get_vertices();
	vector<vector<int>> iget_vertices();
	vector<LabelledState> iget_labelled_vertices();

	// set methods
	void set_verbosity(unsigned level);
	void set_parameters(double lbnd = 0, double ubnd = 1, unsigned max_iter = 50, double precision = 1e-6);

	// output methods
	void write_glpk_output(const string outfile);
	void write_sol(const string outfile);
	void write_constraint_matrix(const string outfile);
	void write_dense_constraint_matrix(const string outfile);
	void write_labels(const string outfile);
	void print_parameters(bool all=false);
};






#endif