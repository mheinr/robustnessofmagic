#ifndef SYMPLECTIC_H
#define SYMPLECTIC_H
#include <initializer_list>
#include <vector>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <set>
#include <cstdint>

#ifndef UTILITIES_H
#include "utilities.h"
#endif

using namespace std;

// ---------------------------------------------------
// ------ Header file for phase space functions ------
// ---------------------------------------------------
//
// Throughout this code, binary vectors in the n-qubit phase space Z_2^{2n} are represented as unsigned integers using their binary representation, e.g.
// 		(1,0,0,1) \in Z_2^4 ----> 1001 = 9
// We use the coordinate convention (z_1,x_1,z_2,x_2,...,z_n,x_n) in phase space such that Z_2^{2n} decomposes into a direct sum of 1-qubit phase spaces.
// Matrices are represented as a vector of unsigned integers, where every entry of this vector corresponds to a row of the matrix.

// Caution!
// --------
// 	Due to the use of __builtin_popcount(), this code can only be used with the GNU compiler suite!
// 	Furthermore, the code uses at least the C++14 standard.


// ---- typedefs

// type that is used to represent binary vectors in phase space
// note that for n qubits, the binary vector corresponds to a sequence of 2n bits, so we use by default uint64_t which can represent 64 bits, so works definitely for up to n=32
typedef uint64_t binvec;
typedef vector<binvec> symplectic_matrix; 


// ----- bit manipulation for operating on binary vectors

inline unsigned popcount(binvec i) {
	return __builtin_popcountll(i);
}

inline unsigned parity(binvec i) {
	return __builtin_parityll(i);
}


// get j-th bit of b, counted from the end of the bitstring, i.e. from the left
inline binvec get_bit(binvec b, unsigned j, unsigned n) {
	return ((b >> ((n)-(j)-1U)) & 1U);
}

// get j-th bit of b, counted from the from the right
inline binvec get_bit2(binvec b, unsigned j) {
	return ((b >> j) & 1U);
}

inline binvec get_bits(binvec b, unsigned j, unsigned k, unsigned n) {
	return ((b >> (n-k-1U)) & ((1U << (k-j+1U))-1));
}


inline binvec set_bit(binvec b, unsigned j, unsigned n) {
	return (b | (1 << (n-j-1U)));
}

inline binvec reset_bit(binvec b, unsigned j, unsigned n) {
	return (b & (~(1 << (n-j-1U))));
}

string write_bits(binvec b, unsigned n);
string write_trits(binvec t, unsigned n);
binvec invert_bits(const binvec b, const unsigned n);

// ---- For working on finite fields

// actually returns the modulus instead of remainder
// e.g mod(-2,3) == 1 instead of (-2)%3 = -2
inline int mod(int x, int m) {
    return (x%m + m)%m;
}

// ---- Phase space methods

unsigned eta(const binvec a, const binvec b, const unsigned n);
int phi(const binvec a, const binvec b, const unsigned n);
int phi(const vector<binvec> &a, const unsigned n);

inline unsigned symplectic_form(const binvec a, const binvec b, const unsigned n) {
	return ( eta(a,b,n) + eta(b,a,n) )%2;
}

// This is the convolution p3 = p1 * p2 in (Z_2)^N (addition mod 2)
int convolve_mod2(double *p1, double *p2, double *p3, const unsigned N);
binvec matrix_vector_prod_mod2(const binvec *A, const binvec x, const unsigned N);
binvec matrix_vector_prod_mod2(const vector<binvec> &A, const binvec x);
vector<binvec> direct_sum(const vector<binvec> &A1, const vector<binvec> &A2);
vector<binvec> direct_sum(const vector<vector<binvec>> &Alist);
vector<binvec> ut_to_dense_matrix(const binvec A, unsigned n);
binvec dense_to_ut_matrix(vector<binvec> &A);

// direct sum of two upper triangle matrices
binvec direct_sum_ut(const binvec A1, const unsigned n1, const binvec A2, const unsigned n2);
void symplectic_transform(double *pin, double *pout, const binvec *S, const unsigned n);
binvec transvection(const binvec h, const binvec x, const binvec n);
vector<binvec> coord_matrix(const unsigned n);
binvec zx_to_product_coordinates(const binvec x, const unsigned n);
binvec product_to_zx_coordinates(const binvec x, const unsigned n);

// counts weights of the Pauli operator that corresponds to the phase space point a
// note that the order is 1,X,Z,Y
vector<unsigned> count_weights(const binvec a, const unsigned n);


// ---- Symplectic group generation

int phase_function(const binvec a, const binvec *S, const unsigned n);
int phase_function(const binvec a, const vector<binvec> &S);
vector<binvec> find_transvection(const binvec x, const binvec y, const unsigned n);
vector<binvec> generate_symplectic_matrix(const binvec i, const unsigned n);
vector<int> liouville_matrix(const vector<binvec> &S, const binvec c=0);


#endif