#ifndef UTILITIES_H
#define UTILITIES_H
#include <initializer_list>
#include <vector>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <random>
#include <tuple>
#include <cblas.h>
#include <cstdint>
#include <algorithm>
#include <numeric>
#include <complex>


using namespace std;

typedef complex<double> cplx;
template<typename T> constexpr T pi = T(3.1415926535897932385);

// ---------------------------------------------------
// ------ Utilities header file ------
// ---------------------------------------------------


// --- data type for points
template <class T>
struct LabelledObject {
	T object;
	string label;

	LabelledObject() {};

	bool operator<(const LabelledObject<T>& rhs)
    {
        return this->object < rhs.object;
    }

    bool operator==(const LabelledObject<T>& rhs)
    {
        return this->object == rhs.object;
    }

    inline bool operator> (const LabelledObject<T>& rhs){ return rhs < (*this); }
	inline bool operator<=(const LabelledObject<T>& rhs){ return !((*this) > rhs); }
	inline bool operator>=(const LabelledObject<T>& rhs){ return !((*this) < rhs); }

};

// --- data type for stabiliser states (density matrices)
struct LabelledState : public LabelledObject<vector<int>> {

	LabelledState() {}

	LabelledState(unsigned n) {
		this->object = vector<int>(n,0);
	}

	LabelledState(initializer_list<int> list, string lab) {
		this->object.assign(list);
		this->label = lab;
	}

	LabelledState(vector<int> vec, string lab) {
		this->object = vec;
		this->label = lab;
	}

	unsigned size() const {
		return this->object.size();
	}

	void clear() {
		this->object.assign(this->object.size(),0);
		this->label.clear();
	}
};

// --- data type for stabiliser state vectors (Hilbert space vectors)
struct LabelledStateVector : public LabelledObject<vector<cplx>> {

	LabelledStateVector() {}

	LabelledStateVector(unsigned n) {
		this->object = vector<cplx>(n,0);
	}

	LabelledStateVector(unsigned n, string lab) {
		this->object = vector<cplx>(n,0);
		this->label = lab;
	}

	LabelledStateVector(initializer_list<cplx> list, string lab) {
		this->object.assign(list);
		this->label = lab;
	}

	LabelledStateVector(vector<cplx> vec, string lab) {
		this->object = vec;
		this->label = lab;
	}

	unsigned size() const {
		return this->object.size();
	}

	void clear() {
		this->object.assign(this->object.size(),0);
		this->label.clear();
	}
};



// ----- helpers


// get all descending partitions of a positive number n, i.e. a list { (n), (n-1,1), ... } of a list of positive integers l_i such that 
// n = l_1 + ... + l_m (m=1,...,n), and
// n >= l_1 >= l_2 >= ... >= l_m >= 1
// The first element is n itself.
vector<vector<unsigned>> get_partitions(unsigned n);
unsigned factorial(const unsigned n);
unsigned binomial_coeff(const unsigned n, const unsigned k);

// ----- indexing routines

/* Computes the multi index for a given linear index assuming row-major order 
	The array dimensions are assumed to be N_1 x N_2 x ... x N_k and are specified by arr_dim = k and the array arr_ranges = {N_1, ..., N_k}. The result is stored in indices (should be allocated before the use of this function!)
*/
unsigned get_multi_index(const unsigned arr_dim, const unsigned* arr_ranges, const unsigned index, unsigned* indices);
unsigned get_multi_index(const vector<unsigned> &arr_ranges, const unsigned index, vector<unsigned> &indices);

// overload where all N_i are assumed to be the same
unsigned get_multi_index(const unsigned arr_dim, const unsigned arr_range, const unsigned index, unsigned* indices);
unsigned get_multi_index(const unsigned arr_dim, const unsigned arr_range, const unsigned index, vector<unsigned> &indices);

// inverse stuff

unsigned get_linear_index(const unsigned arr_dim, const unsigned* arr_ranges, const unsigned* indices);
unsigned get_linear_index(const unsigned arr_dim, const unsigned* arr_ranges, const initializer_list<unsigned> &indices);
unsigned get_linear_index(const unsigned arr_dim, const initializer_list<unsigned> &arr_ranges, const initializer_list<unsigned> &indices);

// overloads where all N_i are assumed to be the same
unsigned get_linear_index(const unsigned arr_dim, const unsigned arr_range, const unsigned* indices);
unsigned get_linear_index(const unsigned arr_dim, const unsigned arr_range, const initializer_list<unsigned> &indices);


// indexing for symmetric matrices, upper triangle storage
inline unsigned get_ut_index(const unsigned i, const unsigned j, const unsigned n) {
	return ( (n*(n-1)/2) - (n-i)*((n-i)-1)/2 + j - i - 1 );
}

inline unsigned get_ut_row(const unsigned k, const unsigned n) {
	return n - 2 - floor(sqrt(-8*k + 4*n*(n-1)-7)/2.0 - 0.5);
}

inline unsigned get_ut_col(const unsigned i, const unsigned k, const unsigned n) {
	return k + i + 1 - n*(n-1)/2 + (n-i)*((n-i)-1)/2;
}

// this is for column-wise storage
inline unsigned get_ut_index2(const unsigned i, const unsigned j, const unsigned n) {
	return ( i + j*(j-1)/2 );
}

unsigned get_ut_col2(const unsigned k, const unsigned n);

inline unsigned get_ut_row2(const unsigned j, const unsigned k, const unsigned n) {
	return (k - j*(j-1)/2);
}


// ----- input / output

unsigned get_number_of_lines(string filename);

#endif