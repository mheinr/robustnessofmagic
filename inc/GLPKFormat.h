#ifndef GLPKFORMAT_H
#define GLPKFORMAT_H
#include <fstream>
#include <iostream>
#include <vector>


#ifndef UTILITIES_H
#include "utilities.h"
#endif

using namespace std;

// ----- GLPK utilities

// data struct for GLPK
struct GLPKFormat {
	vector<int> rows;
	vector<int> cols;
	vector<double> values;
	unsigned non_zeros;
	unsigned nrows;
	unsigned ncols;
};

// reads sparse matrix in COO format and saves to struct
GLPKFormat to_GLPK_format(const string cmatrix_file, bool transpose=false);

// reads dense list of row vectors and saves to struct
GLPKFormat to_GLPK_format(const vector<vector<int>>& matrix,  bool transpose=false);
GLPKFormat to_GLPK_format(const vector<LabelledState>& matrix, bool transpose=false);

void print(const GLPKFormat& data);

#endif