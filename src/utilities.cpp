#ifndef UTILITIES_H
#include "utilities.h"
#endif


// ---------------------------------------------------
// ------ Utilities file ------
// ---------------------------------------------------


// ----- helpers


// get all descending partitions of a positive number n, i.e. a list { (n), (n-1,1), ... } of a list of positive integers l_i such that 
// n = l_1 + ... + l_m (m=1,...,n), and
// n >= l_1 >= l_2 >= ... >= l_m >= 1
// The first element is n itself.
vector<vector<unsigned>> get_partitions(unsigned n) {
	if( n == 1 ) {
		// return the list { (1) }
		return vector<vector<unsigned>> (1, vector<unsigned>(1,1) );
	}

	vector<vector<unsigned>> ret = { vector<unsigned>(1,n) };
	vector<vector<unsigned>> ret2;
	
	for(unsigned k=1; k<n; k++) {
		ret2 = get_partitions(k);
		for(auto p : ret2) {
			if(p.at(0) <= n-k) {
				p.insert(p.begin(), n-k);
				ret.push_back(p);
			}
		}
	}

	return ret;
}

unsigned factorial(const unsigned n) {
	unsigned f = 1;
	for(unsigned nn=n; nn>0; --nn) {
		f *= nn;
	}
	return f;
}

unsigned binomial_coeff(const unsigned n, const unsigned k) {
    vector<unsigned> C(k+1,0);
 
    C[0] = 1;
 
    for (unsigned i = 1; i <= n; i++) {
        // Compute next row of pascal triangle using
        // the previous row
        for (unsigned j = min(i, k); j > 0; j--)
            C[j] = C[j] + C[j-1];
    }
    return C[k];
}


// ----- indexing routines

/* Computes the multi index for a given linear index assuming row-major order 
	The array dimensions are assumed to be N_1 x N_2 x ... x N_k and are specified by arr_dim = k and the array arr_ranges = {N_1, ..., N_k}. The result is stored in indices (should be allocated before the use of this function!)
*/
unsigned get_multi_index(const unsigned arr_dim, const unsigned* arr_ranges, const unsigned index, unsigned* indices) {

	unsigned mod;
	unsigned ind = index;
	if(arr_dim> 1) {
		for(unsigned i=arr_dim-1; i>0; i--) {
			mod = ind % arr_ranges[i];
			indices[i] = mod;
			ind = (ind - mod) / arr_ranges[i];
		}
	}
	indices[0] = ind;

	return 0;
}

unsigned get_multi_index(const vector<unsigned> &arr_ranges, const unsigned index, vector<unsigned> &indices) {
	indices.reserve(arr_ranges.size());
	return get_multi_index(arr_ranges.size(), arr_ranges.data(), index, indices.data());
}

// overload where all N_i are assumed to be the same
unsigned get_multi_index(const unsigned arr_dim, const unsigned arr_range, const unsigned index, unsigned* indices) {
	vector<unsigned> vec (arr_dim, arr_range);
	return get_multi_index(arr_dim, vec.data(), index, indices);
}

unsigned get_multi_index(const unsigned arr_dim, const unsigned arr_range, const unsigned index, vector<unsigned> &indices) {
	vector<unsigned> vec (arr_dim, arr_range);
	indices.reserve(arr_dim);
	return get_multi_index(arr_dim, vec.data(), index, indices.data());
}

// inverse stuff

unsigned get_linear_index(const unsigned arr_dim, const unsigned* arr_ranges, const unsigned* indices) {
	unsigned index = 0;
	unsigned jprod;
	for(unsigned i=0; i<arr_dim; i++) {//change order?
		jprod = 1;
		for(unsigned j=i+1; j<arr_dim; j++) {
			jprod *= arr_ranges[j];
		}
		jprod *= indices[i];
		index += jprod;
	}

	return index;
}

unsigned get_linear_index(const unsigned arr_dim, const unsigned* arr_ranges, const initializer_list<unsigned> &indices){
	return get_linear_index(arr_dim, arr_ranges, const_cast<unsigned* >(indices.begin()));
}

unsigned get_linear_index(const unsigned arr_dim, const initializer_list<unsigned> &arr_ranges, const initializer_list<unsigned> &indices){
	return get_linear_index(arr_dim, const_cast<unsigned* >(arr_ranges.begin()), const_cast<unsigned* >(indices.begin()));
}

// overloads where all N_i are assumed to be the same
unsigned get_linear_index(const unsigned arr_dim, const unsigned arr_range, const unsigned* indices) {
	vector<unsigned> vec (arr_dim, arr_range);
	return get_linear_index(arr_dim, vec.data(), indices);
}

unsigned get_linear_index(const unsigned arr_dim, const unsigned arr_range, const initializer_list<unsigned> &indices) {
	vector<unsigned> vec (arr_dim, arr_range);
	return get_linear_index(arr_dim, vec.data(), const_cast<unsigned* >(indices.begin()));
}

unsigned get_ut_col2(const unsigned k, const unsigned n) {
	// return floor( 0.5 + 0.5*sqrt(8*k+1) );
	for(unsigned j=1; j<n; j++) {
		if(k >= j*(j-1)/2 && k < j*(j+1)/2) {
			return j;
		}
	}
}



// ----- input / output

unsigned get_number_of_lines(string filename) {
	unsigned n=0;
	string line;
	fstream fin(filename, ios::in);
	if(fin.is_open()) {
		while(getline(fin, line)) {
			++n;
		}
	}
	fin.close();
	return n;
}