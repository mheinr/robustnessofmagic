#ifndef STABILISER_H
#include "stabiliser.h"
#endif


// -----------------------------------
// ---- Helper functions
// -----------------------------------

// computes purity of state in the H-symmetric Fock basis, with identity component omitted
// this is not the actual purity, since we multiplied it with an overall 2^(-n) for numerical reasons
double purity(const vector<int>& state) {
	unsigned n = state.size();
	double ret = 1.;
	for(unsigned k=0; k<n; k++) {
		ret += (double)state.at(k) * (double)state.at(k) / ( pow(2., k+1) * (double)binomial_coeff(n,k+1) );
	}
	return ret;
}

double purity(const LabelledState& state) {
	return purity(state.object);
}

// computes trace inner product in the H-symmetric Fock basis up to an overall constant of 2^(-n)
double trace_inner_product(const vector<double>& state1, const vector<double>& state2) {
	unsigned n = state1.size();
	assert(n==state2.size());
	double ret = 1.;
	for(unsigned k=0; k<n; k++) {
		ret += state1.at(k) * state2.at(k) / ( pow(2., k+1) * (double)binomial_coeff(n,k+1) );
	}
	return ret;
}


// applies a Pauli matrix to the state vector in and returns the result as out. The Pauli matrix is identified by 
// 	i^{-a_1\cdot a_2} Z(a_1) X(a_2)  with indices a=(a_1,a_2) \in \Z_2^{2n}
// additionally, an optional phase can be specified which is applied to all elements.
void apply_Pauli(const unsigned n, const binvec a, const vector<cplx> &in, vector<cplx> &out, const cplx phase=1) {

	// isolate a_1 and a_2
	binvec a_1 = get_bits(a, 0, n-1, 2*n);
	binvec a_2 = get_bits(a, n, 2*n-1, 2*n);
	binvec len = in.size();

	// cout << write_bits(a_1, n) << " " << write_bits(a_2, n) << endl;

	// X affects a translation by a_2 and Z will add only a phase
	cplx im = 1;
	unsigned m = popcount( a_1 & a_2 ) % 4;
	switch (m) {
		case 0: im = 1; break;
		case 1: im = -1i; break;
		case 2: im = -1; break;
		case 3: im = 1i; break;
	}

	for(binvec x=0; x<len; x++) {
		out.at(x) = in.at(x^a_2) * pow(-1, parity( a_1 & x )) * im * phase;
	}
}



// ------ does not work (this or the other routines!!)
// finds the state representation of a stabiliser state given the density matrix in the Pauli basis (i.e. the +1 eigenvector of the projector)
LabelledStateVector find_state_vector(const LabelledState &state) {
	// The algorithm works as follows:
	// We compute the generators g_1,...,g_n with signs s_1,...,s_n of the stabiliser group of state, given its label. Next, we apply the projectors (id + s_i g_i )/2 sequentially to an initial vector. This vector is drawn randomly to increase the chance that it is not in the orthocomplement of the state.
	// This procedure can result in 0. In this case, we draw another vector and repeat

	// read label
	istringstream iss(state.label);
	vector<string> words {istream_iterator<string>{iss}, istream_iterator<string>{}};

	unsigned n = get_order( words.at(0) );
	vector<binvec> L = graph_Lagrangian( graph6_to_adj_mat(words.at(0)), n );
	vector<unsigned> signs (n,0);

	vector<symplectic_matrix> local_sp = { symplectic_matrix({0b10,0b01}), symplectic_matrix({0b01,0b10}), symplectic_matrix({0b11,0b01}), symplectic_matrix({0b01,0b11}), symplectic_matrix({0b11,0b10}), symplectic_matrix({0b10,0b11}) };
	vector<symplectic_matrix> Slist (n);

	for(unsigned i=0; i<n; i++) {
		signs.at(i) = words.at(2).at(i) - '0';
		Slist.at(i) = local_sp.at( words.at(1).at(i) - '0' );
	}

	symplectic_matrix S = direct_sum(Slist);

	// compute the generators and convert them in z-x coordinates
	binvec t = 0;
	// cout << "   graph Lagrangian:" << endl;
	// for(unsigned k=0; k<n; k++) {
	// 	cout << write_bits(L.at(k), 2*n) << endl; // graph Lagrangian
	// }
	// cout << "   stab Lagrangian:" << endl;
	for(unsigned k=0; k<n; k++) {
		t = matrix_vector_prod_mod2(S, L.at(k));
		// cout << write_bits(t, 2*n) << endl; // stabiliser Lagrangian
		L.at(k) = product_to_zx_coordinates( t, n );
	}


	// prepare temporary and initial vectors
	unsigned N = pow(2,n);
	vector<cplx> temp (N, 0.0);

	LabelledStateVector out (N, state.label);

	// initialise random number generation
	random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.1, 1.0);
    

	// apply projections
	double out_norm = 0;
	unsigned counter = 0;

	while(out_norm < 1e-8 && counter < 5) {
		// generate random entries for out (not Haar-random but that does not matter here)
		for(unsigned i=0; i<N; i++) {
    		out.object.at(i) = dis(gen);
    	}

		for(unsigned k=0; k<n; k++) {
			// this seperates the z from the x part
			apply_Pauli( n, L.at(k), out.object, temp, (signs.at(k)!=0) ? -1 : 1 );

			// projection, i.e. compute (out+temp)/2
			transform( out.object.begin(), out.object.end(), temp.begin(), out.object.begin(), [](auto &x, auto &y) { return (x+y)/2.0; });
		}

		// compute final norm
		out_norm = accumulate( out.object.begin(), out.object.end(), 0.0, [](double &x, cplx &y){ return x+norm(y); });
		out_norm = sqrt(out_norm);

		if(out_norm < 1e-8) {
			cout << "find_state_vector(): Projection resulted in a vector close to zero. Norm: " << out_norm << endl;
			cout << "find_state_vector(): Will retry with different initial vector." << endl;
			counter++;
		}
	}

	// renormalise and phase convention 

	// convention here is the that the component with least significiant bit gets a 1
	cplx ph = *find_if( out.object.begin(), out.object.end(), [](auto &x) { return (norm(x) > 1e-8); } ); // find first non-zero entry

	transform( out.object.begin(), out.object.end(), out.object.begin(), [ph](auto &x) { return x/ph; });

	out_norm = accumulate( out.object.begin(), out.object.end(), 0.0, [](double &x, cplx &y){ return x+norm(y); });
	out_norm = sqrt(out_norm);

	transform( out.object.begin(), out.object.end(), out.object.begin(), [out_norm](auto &x) { return x/out_norm; });

	return out;
}

LabelledStateVector find_fs_state_vector(const LabelledState &state) {
	// The algorithm works as follows:
	// We compute the generators g_1,...,g_n with signs s_1,...,s_n of the stabiliser group of state, given its label. Next, we apply the projectors (id + s_i g_i )/2 sequentially to an initial vector. This vector is drawn randomly to increase the chance that it is not in the orthocomplement of the state.
	// This procedure can result in 0. In this case, we draw another vector and repeat

	// read label
	istringstream iss(state.label);
	vector<string> words {istream_iterator<string>{iss}, istream_iterator<string>{}};

	unsigned n = get_order( words.at(0) );
	vector<binvec> L = graph_Lagrangian( graph6_to_adj_mat(words.at(0)), n );
	vector<unsigned> signs (n,0);

	vector<symplectic_matrix> local_sp = { symplectic_matrix({0b10,0b01}), symplectic_matrix({0b11,0b01}), symplectic_matrix({0b01,0b11}) };
	vector<symplectic_matrix> Slist (n);

	for(unsigned i=0; i<n; i++) {
		signs.at(i) = words.at(2).at(i) - '0';
		Slist.at(i) = local_sp.at( words.at(1).at(i) - '0' );
	}

	symplectic_matrix S = direct_sum(Slist);

	// compute the generators and convert them in z-x coordinates
	binvec t = 0;
	for(unsigned k=0; k<n; k++) {
		// cout << write_bits(L.at(k), 2*n) << endl;
		t = matrix_vector_prod_mod2(S, L.at(k));
		L.at(k) = product_to_zx_coordinates( t, n );
	}


	// prepare temporary and initial vectors
	unsigned N = pow(2,n);
	vector<cplx> temp (N, 0.0);

	LabelledStateVector out (N, state.label);

	// initialise random number generation
	random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(0.1, 1.0);
    

	// apply projections
	double out_norm = 0;
	unsigned counter = 0;

	while(out_norm < 1e-8 && counter < 5) {
		// generate random entries for out (not Haar-random but that does not matter here)
		for(unsigned i=0; i<N; i++) {
    		out.object.at(i) = dis(gen);
    	}

		for(unsigned k=0; k<n; k++) {
			// this seperates the z from the x part
			apply_Pauli( n, L.at(k), out.object, temp, (signs.at(k)!=0) ? -1 : 1 );

			// projection, i.e. compute (out+temp)/2
			transform( out.object.begin(), out.object.end(), temp.begin(), out.object.begin(), [](auto &x, auto &y) { return (x+y)/2.0; });
		}

		// compute final norm
		out_norm = accumulate( out.object.begin(), out.object.end(), 0.0, [](double &x, cplx &y){ return x+norm(y); });
		out_norm = sqrt(out_norm);

		if(out_norm < 1e-8) {
			cout << "find_state_vector(): Projection resulted in a vector close to zero. Norm: " << out_norm << endl;
			cout << "find_state_vector(): Will retry with different initial vector." << endl;
			counter++;
		}
	}

	// renormalise and phase convention 

	// convention here is the that the component with least significiant bit gets a 1
	cplx ph = *find_if( out.object.begin(), out.object.end(), [](auto &x) { return (norm(x) > 1e-8); } ); // find first non-zero entry

	transform( out.object.begin(), out.object.end(), out.object.begin(), [ph](auto &x) { return x/ph; });

	out_norm = accumulate( out.object.begin(), out.object.end(), 0.0, [](double &x, cplx &y){ return x+norm(y); });
	out_norm = sqrt(out_norm);

	transform( out.object.begin(), out.object.end(), out.object.begin(), [out_norm](auto &x) { return x/out_norm; });

	return out;
}


// -----------------------------------
// ---- Magic state representation
// -----------------------------------

// Representation of H state
vector<double> H = {1,1/sqrt(2),0,1/sqrt(2)};

vector<double> H_state(const unsigned n, const double p) {
	vector<double> ret (pow(4,n)-1, 0.);
	vector<unsigned> ind (n);

	for(unsigned i=1; i<pow(4,n); i++) {
		get_multi_index(n, 4, i, ind);
		ret.at(i-1) = 1-p;
		for(unsigned j=0; j<n; j++) {
			ret.at(i-1) *= H.at(ind.at(j));
		}
	}

	return ret;
}

// H state in the (renormalised) number / Fock basis
vector<double> H_state_nb(const unsigned n, const double p) {
	vector<double> ret (n);
	for(unsigned k=1; k<n+1; k++) {
		ret.at(k-1) = (1-p)*pow(sqrt(2),k)*binomial_coeff(n,k);
	}
	return ret;
}

vector<double> T_state_nb(const unsigned n, const double p) {
	vector<double> ret (n);
	for(unsigned k=1; k<n+1; k++) {
		ret.at(k-1) = (1-p)*pow(sqrt(3),k)*binomial_coeff(n,k);
	}
	return ret;
}


// Magic state generation (Calderbank method, not complete)
// diagonal gates in the 3rd level of the hierarchy as symmetric matrices over 2^3
vector<LabelledStateVector> gen_magic_states(const unsigned n) {

	unsigned N = 1 << n; // 2^n
	unsigned M = n*(n+1)/2;
	unsigned K = pow(8, M);
	unsigned prod = 0;
	double norm = 1./sqrt(N);

	// return vector
	vector<LabelledStateVector> ret(K, LabelledStateVector(N));

	// this will be correspond to a symmetric matrix over Z_{2^3}=Z_8
	vector<unsigned> A (M, 0);
	string label;

	// loop over symmetric matrices
	for(unsigned x=0; x<K; x++) { 
		label.clear();

		// convert x into an octal representation
		get_multi_index(M, 8, x, A);

		// set label
		for(unsigned i=0; i<M; i++) {
			label += to_string(A.at(i));
		}

		// set label
		ret.at(x).label = label;

		// evaluate v^T A v mod 8 for all binary vectors v 
		for(unsigned v=0; v<N; v++) {
			prod = 0;

			// evaluate the quadratic form (mod 8), but iterate only over the lower triangular part of the matrix (times 2) and the diagonal
			for(unsigned i=0; i<n; i++) {
				for(unsigned j=0; j<i; j++) {
					prod += get_bit(v, i, n) * get_bit(v, j, n) * A.at( i*(i+1)/2 + j );
				}
				prod *= 2;
				prod += get_bit(v, i, n) * A.at( i*(i+1)/2 + i);
			}

			ret.at(x).object.at(v) = exp(1.i * pi<cplx>/4. * (cplx)prod); // 8-th root of unity raised to prod
			ret.at(x).object.at(v) *= norm;
		}
	}

	return ret;
}


// -----------------------------------
// ---- Stabiliser generation
// -----------------------------------

// --- graph routines

// read graph6 format and returns the adjacency matrix in as a vector of bitstrings representing the rows
// IMPORTANT: this implementation is quick and dirty and hence only works for graphs with less than 63 vertices (fine for me)
vector<binvec> graph6_to_adj_mat(const string g6) {
	// The input string g6 is assumed to hold a graph6 representation of the graph in ASCII-encoded form, i.e. it's a sequence of ASCII characters

	// interprete g6 as integer
	unsigned n = (unsigned)g6.at(0)-63;
	unsigned l = g6.size();
	unsigned N = n*(n-1)/2;
	unsigned i,j;
	vector<binvec> ret (n,0);
	vector<binvec> x(l-1,0);

	if(N > 0) {
		for(i=1; i<l; i++) {
			x.at(i-1) = (binvec)(g6.at(i)-63);
		}

		// read out the bits and fill the adjacency matrix		
		for(unsigned k=0; k<N; k++) {
			if(get_bit(x.at(k/6), k%6, 6) == 1) {
				j = get_ut_col2(k,n);
				i = get_ut_row2(j,k,n);
				ret.at(i) = set_bit(ret.at(i), j, n);
				ret.at(j) = set_bit(ret.at(j), i, n);
			}
		}
	}

	return ret;
}

// inverse operation
string adj_mat_to_graph6(const vector<binvec> &A) {
	unsigned n = A.size();

	assert(n < 63);

	if(n == 0 || n == 1) {
		return string(1, 63+n);
	}

	// convert upper triangle of A to a column-wise ordered binary vector which is split in blocks of 6 bits each
	unsigned N = n*(n-1)/2;
	unsigned m = N/6;
	unsigned k;

	if(N%6 != 0) {
		++m;
	}
	vector<binvec> x(m,0);

	for(unsigned i=0; i<n; i++) {
		for(unsigned j=i+1; j<n; j++) {
			if(get_bit(A.at(i),j,n) == 1) {
				k = get_ut_index2(i,j,n);
				x.at(k/6) = set_bit(x.at(k/6), k%6, 6);
			}
		}
	}

	// now convert x to a string of ASCII characters
	// first character encodes n
	string ret (m+1, 63);	
	ret.at(0) = (char)(n+63);

	for(unsigned k=0; k<m; k++) {
		ret.at(k+1) = (char)( x.at(k) + 63 );
	}

	return ret;
}

unsigned get_order(const string g6) {
	return (unsigned)g6.at(0)-63;
}

unsigned get_order_from_file(const string file) {
	// open the file
	fstream fin(file, ios::in);
	string row;

	if(fin.is_open()) {
		fin >> row;
	} 
	else {
		cout << "Couldn't open file " << file << endl;
		return 1;
	}
	fin.close();

	return get_order(row);
}

// converts a binary vector x of length n(n-1)/2 into a symmetric matrix with zeros on the diagonal
// we assume that x represents a column-wise ordering of the upper triangle of the matrix
// A is given as a vector of rows, represented as bitstrings
vector<binvec> gen_adj_matrix(const binvec x, unsigned n) {
	vector<binvec> A (n,0);
	unsigned N = n*(n-1)/2;
	unsigned i,j;

	// read out the bits and fill the adjacency matrix	
	for(unsigned k=0; k<N; k++) {
		if(get_bit(x, k, N) == 1) {
			j = get_ut_col2(k,n);
			i = get_ut_row2(j,k,n);
			A.at(i) = set_bit(A.at(i), j, n);
			A.at(j) = set_bit(A.at(j), i, n);		}
	}

	return A;
}

vector<binvec> gen_sym_matrix(const binvec x, unsigned n) {
	vector<binvec> A (n,0);
	unsigned N = n*(n+1)/2;
	unsigned i,j;

	// read out the bits and fill the adjacency matrix	
	for(unsigned k=0; k<N; k++) {
		if(get_bit(x, k, N) == 1) {
			j = get_ut_col2(k,n+1) - 1;
			i = get_ut_row2(j,k,n+1) - 1;
			A.at(i) = set_bit(A.at(i), j, n);
			A.at(j) = set_bit(A.at(j), i, n);		}
	}

	return A;
}


// --- graph states

// represents generator matrix of a graph state
vector<binvec> graph_Lagrangian(const vector<binvec> &A, const unsigned n) {
	// This returns the representation of a graph state given in terms of a basis for the Lagrangian subspace. The graph of the graph state is specified by its adjacency matrix, given as a vector A of bitstring representing the row vectors in Z_2^n.
	// The basis vectors are given by the column vectors of the matrix
	// 			(  A  )
	// 		B = ( --- )			(in (z_1,...,z_n,x_1,...,x_n) coordinates)
	//			(  I  )
	// where A is the adjacency matrix, now interpreted as nxn symmetric matrix and I is the nxn identity matrix. Note that since we are working in product coordinates (z_1,x_1,z_2,x_2,...,z_n,x_n), the function will return a permuted version of these vectors.

	// reserve memory
	vector<binvec> B(n,0);
	unsigned N = n*(n-1)/2;
	vector<binvec> M = coord_matrix(n);

	// fill B 
	for(unsigned i=0; i<n; i++) {
		 // since A is symmetric, row vectors = column vectors, but we have to shift the bits by n because of the lower identity block
		B[i] = A[i] << n;
	}
	
	for(unsigned j=0; j<n; j++) {
		// identity matrix
		B[j] = set_bit(B[j], n+j, 2*n); 

		// change to product coordinates
		B[j] = matrix_vector_prod_mod2(M, B[j]);
	}

	return B;
}


// --- stabiliser state from generator matrix

bool in_Lagrangian(const binvec &x, const vector<binvec> &B, const unsigned n) {
	unsigned test = 0;
	for(binvec v : B) {
		test += symplectic_form(x,v,n);
	} 
	return (test == 0);
}

vector<int> stabiliser_state(const vector<binvec> &B, const unsigned s) {
	// Generates the stabiliser state rho(B,s) that corresponds to the Lagrangian with basis B and a choice of signs (-1)^{s_i} where the bits s_i \in \Z_2 are specified in the length-n-bitstring s.
	// The stabiliser state is given in the Pauli basis {W(a)|a\in\Z_2^{2n}} where the only non-vanishing components rho_a corresponds to points a in the Lagrangian subspace and can thus be written in the basis B = (b_1,...,b_n) as
	//		a = \sum_{i=1}^n a_i b_i.
	// These components are explicitly given as
	//		rho_a = (-1)^{\sum_{i=1}^n a_i s_i} i^{\phi(a_1 b_1,...,a_n b_n)},
	// where \phi is the phase function appearing in the composition law of Pauli operators (see above).

	unsigned n = B.size();
	vector<int> state (pow(4,n)-1,0); // 0th component is always 1
	vector<binvec> avec (n);
	unsigned phase, idx;

	// loop over all points a in the Lagrangian (except 0)
	for(binvec a=1; a<pow(2,n); a++) {
		idx = 0;

		for(unsigned i=0; i<n; i++) {
			avec.at(i) = get_bit(a,i,n) * B.at(i);
			idx ^= avec.at(i); // this gives in the end the coordinates of a w.r.t. the canonical basis
		}

		// compute phi
		phase = mod(phi(avec,n),4);
		assert(phase==0 || phase==2);
		if(phase == 2) {
			phase = 1;
		}
		else {
			phase = 0;
		}

		phase += parity(a&s); // inner product of a and s

		state.at(idx-1) = pow(-1,phase);// / pow(2,n);
	}

	return state;
}


// ---- generation of stabiliser states
//--------------------------------------

// complete local symplectic group
vector<LabelledObject<symplectic_matrix>> local_symplectic_group(unsigned n) {
	unsigned M = pow(6,n);

	vector<symplectic_matrix> local_cosets = { symplectic_matrix({0b10,0b01}), symplectic_matrix({0b01,0b10}), symplectic_matrix({0b11,0b01}), symplectic_matrix({0b01,0b11}), symplectic_matrix({0b11,0b10}), symplectic_matrix({0b10,0b11}) };
	vector<LabelledObject<symplectic_matrix>> LC_cosets (M);
	vector<symplectic_matrix> Slist (n);

	vector<unsigned> dits(n, 0);
	for(binvec i=0; i<M; i++) {
		get_multi_index(n, 6, i, dits);		
		for(unsigned j=0; j<n; j++) {
			Slist.at(j) = local_cosets.at(dits.at(j));
			LC_cosets.at(i).label += to_string(dits.at(j));
		}
		LC_cosets.at(i).object = direct_sum(Slist);
	}

	return LC_cosets;
}

vector<LabelledObject<symplectic_matrix>> diag_local_symplectic_group(unsigned n) {
	unsigned M = pow(2,n);

	vector<symplectic_matrix> local_cosets = { symplectic_matrix({0b10,0b01}), symplectic_matrix({0b11,0b01}) };
	vector<LabelledObject<symplectic_matrix>> LC_cosets (M);
	vector<symplectic_matrix> Slist (n);

	vector<unsigned> dits(n, 0);
	for(binvec i=0; i<M; i++) {
		get_multi_index(n, 2, i, dits);		
		for(unsigned j=0; j<n; j++) {
			Slist.at(j) = local_cosets.at(dits.at(j));
			LC_cosets.at(i).label += to_string(dits.at(j));
		}
		LC_cosets.at(i).object = direct_sum(Slist);
	}

	return LC_cosets;
}

int num_stabiliser_states(unsigned n) {
	unsigned ret = pow(2,n);
	for(unsigned i=1; i<=n; i++) {
		ret *= pow(2,i)+1;
	}
	return ret;
}

// These routines generate stabiliser states based on graphs. To get the full set of stabiliser states, we take the following orbits of the corresponding graph states
//	1. W.r.t. to the local symplectic group Sp(2,Z_2)
//  2. W.r.t. to the signs of the n generators


// this generates graphs internally
int stabiliser_states(const unsigned n, vector<LabelledState> &states) {

	// outer loop variables
	unsigned N = pow(2,n);
	unsigned NN = pow(2,n*n);
	unsigned D = N*N;
	unsigned gn = n*(n-1)/2;
	unsigned graph_len = pow(2,gn);
	string graph_label;

	states.reserve(NN); // estimated size
	vector<binvec> A (n);
	vector<binvec> B (n);
	vector<binvec> SB (n);

	// inner Lagrangian loop variables
	LabelledState state (D-1); // 0th component is ommited since it is forced to be 1 
	vector<binvec> Lagrangian_coord (N-1);
	vector<int> Lagrangian_phase (N-1);
	vector<binvec> avec (n,0);
	int phase;
	binvec aa;

	auto SP = local_symplectic_group(n);
	unsigned SP_size = SP.size();

	// graph state loop
	cout << "  Compute " << n << " qubit stabiliser states for graph " << endl;

	for(unsigned g=0; g<graph_len; g++) {

		cout << "\r    #" << g+1 << " / " << graph_len << flush;

		// get Lagrangian from adjacency matrix
		A = gen_adj_matrix(g, n);
		graph_label = adj_mat_to_graph6(A);
		B = graph_Lagrangian(A, n);

		// we have to take the local symplectic orbit of that Lagrangian
		for(binvec h=0; h<SP_size; h++) {

			// compute image of the Lagrangian
			for(unsigned k=0; k<n; k++) {
				SB.at(k) = matrix_vector_prod_mod2(SP.at(h).object, B.at(k));
			}

			// loop over all points a in the Lagrangian SB (except 0) and fill the vector Lagrangian_data
			// Lagrangian_coord.clear();
			// Lagrangian_phase.clear();

			for(binvec a=1; a<N; a++) {
				aa = 0;

				// get coordinates w.r.t. the canonical basis
				for(unsigned i=0; i<n; i++) {
					avec.at(i) = get_bit(a,i,n) * SB.at(i);
					aa ^= avec.at(i); 
				}

				// compute the "Lagrangian" phase phi which is i^phi with phi=0,2,4,...
				// we will write it as i^phi = (-1)^(phi/2)
				phase = phi(avec,n); 
				phase /= 2; 

				// save for later
				Lagrangian_coord.at(a-1) = aa;
				Lagrangian_phase.at(a-1) = pow(-1,phase);

			}

			// --- now, loop over generator phases and use the Lagrangian data computed before
			for(binvec s=0; s<N; s++) {
				// clear state
				state.clear();

				// note that this loop will only involve non-trivial components
				for(binvec a=1; a<N; a++) {

					// explicit stabiliser phase which is inner product of a and s 
					phase = parity(a & s); 

					// write the coefficient in the Pauli basis
					// cout << Lagrangian_coord.at(a-1) << endl;
					state.object.at( Lagrangian_coord.at(a-1) - 1 ) =  pow(-1,phase) * Lagrangian_phase.at(a-1) ;		
				}

				// add a label
				state.label = graph_label+" "+SP.at(h).label+" "+write_bits(s,n);

				// check if state already exists
				// to do that, we use binary search with std::lower_bound() which gives an iterator on the first element that is not less than pr_state
				// note that LabelledState has an overloaded < operator
				auto it = lower_bound(states.begin(), states.end(), state);

				if(it == states.end() || state < *it) {
					// element was not redundant, so add it to the list
					states.insert(it, state);
				}
				// note that procedures preserves the ordering in states ... 
			}
		}
	}

	cout << endl;

	states.shrink_to_fit();

	return 0;
}
int fs_stabiliser_states(const unsigned n, vector<LabelledState> &states) {

	// outer loop variables
	unsigned N = pow(2,n);
	unsigned NN = pow(2,n*n);
	unsigned D = N*N;
	unsigned gn = n*(n-1)/2;
	unsigned graph_len = pow(2,gn);
	string graph_label;

	states.reserve(NN); // estimated size
	vector<binvec> A (n);
	vector<binvec> B (n);
	vector<binvec> SB (n);

	// inner Lagrangian loop variables
	LabelledState state (D-1); // 0th component is ommited since it is forced to be 1 
	vector<binvec> Lagrangian_coord (N-1);
	vector<int> Lagrangian_phase (N-1);
	vector<binvec> avec (n,0);
	int phase;
	binvec aa;

	auto SP = diag_local_symplectic_group(n);
	unsigned SP_size = SP.size();

	// graph state loop
	cout << "  Compute " << n << " qubit stabiliser states for graph " << endl;

	for(unsigned g=0; g<graph_len; g++) {

		cout << "\r    #" << g+1 << " / " << graph_len << flush;

		// get Lagrangian from adjacency matrix
		A = gen_adj_matrix(g, n);
		graph_label = adj_mat_to_graph6(A);
		B = graph_Lagrangian(A, n);

		// we have to take the local symplectic orbit of that Lagrangian
		for(binvec h=0; h<SP_size; h++) {

			// compute image of the Lagrangian
			for(unsigned k=0; k<n; k++) {
				SB.at(k) = matrix_vector_prod_mod2(SP.at(h).object, B.at(k));
			}

			// loop over all points a in the Lagrangian SB (except 0) and fill the vector Lagrangian_data
			// Lagrangian_coord.clear();
			// Lagrangian_phase.clear();

			for(binvec a=1; a<N; a++) {
				aa = 0;

				// get coordinates w.r.t. the canonical basis
				for(unsigned i=0; i<n; i++) {
					avec.at(i) = get_bit(a,i,n) * SB.at(i);
					aa ^= avec.at(i); 
				}

				// compute the "Lagrangian" phase phi which is i^phi with phi=0,2,4,...
				// we will write it as i^phi = (-1)^(phi/2)
				phase = phi(avec,n); 
				phase /= 2; 

				// save for later
				Lagrangian_coord.at(a-1) = aa;
				Lagrangian_phase.at(a-1) = pow(-1,phase);

			}

			// --- now, loop over generator phases and use the Lagrangian data computed before
			for(binvec s=0; s<N; s++) {
				// clear state
				state.clear();

				// note that this loop will only involve non-trivial components
				for(binvec a=1; a<N; a++) {

					// explicit stabiliser phase which is inner product of a and s 
					phase = parity(a & s); 

					// write the coefficient in the Pauli basis
					// cout << Lagrangian_coord.at(a-1) << endl;
					state.object.at( Lagrangian_coord.at(a-1) - 1 ) =  pow(-1,phase) * Lagrangian_phase.at(a-1) ;		
				}

				// add a label
				state.label = graph_label+" "+SP.at(h).label+" "+write_bits(s,n);

				// check if state already exists
				// to do that, we use binary search with std::lower_bound() which gives an iterator on the first element that is not less than pr_state
				// note that LabelledState has an overloaded < operator
				auto it = lower_bound(states.begin(), states.end(), state);

				if(it == states.end() || state < *it) {
					// element was not redundant, so add it to the list
					states.insert(it, state);
				}
				// note that procedures preserves the ordering in states ... 
			}
		}
	}

	cout << endl;

	states.shrink_to_fit();

	return 0;
}



// state vector generation
vector<LabelledStateVector> fs_stabiliser_state_vectors(const unsigned n) {
	// generate full support stabiliser state vectors directly
	unsigned N = 1 << n;
	unsigned K = 1 << (n*(n-1))/2;
	unsigned idx = 0;
	unsigned q = 0;
	double norm = 1./sqrt(N);

	vector<LabelledStateVector> ret (K*N*N, LabelledStateVector(N));
	vector<int> qq (N);
	vector<int> ll (N);

	// iterate over quadratic forms
	for(binvec A=0; A<K; A++) {

		// evaluate quadratic form
		for(binvec x=0; x<N; x++) {
			q = 0;
			for(unsigned i=0; i<n; i++) {
				for(unsigned j=0; j<i; j++) {
					q += get_bit(x, i, n) * get_bit(x, j, n) * get_bit(A, i*(i+1)/2 + j, K);
				}
			}
			qq.at(x) = (q%2 == 0) ? 1 : -1;
		}


		// iterate over linear parts
		for(binvec l=0; l<N; l++) {

			// evaluate linear form
			for(binvec x=0; x<N; x++) {
				q = parity( x & l );
				ll.at(x) = (q == 0) ? 1 : -1;
			}

			for(binvec d=0; d<N; d++) {
				idx = A + K * (l + N*d);

				// evaluate linear form
				for(binvec x=0; x<N; x++) {
					q = parity( x & d );
					ret.at(idx).object.at(x) = (q == 0) ? 1 : 1.i;
					ret.at(idx).object.at(x) *= ll.at(x) * qq.at(x) * norm;
				}

				ret.at(idx).label = write_bits(A, (n*(n-1))/2) + " " + write_bits(l, n) + " " + write_bits(d, n);
			}
		}
	}

	return ret;
}



int stabiliser_states_old(const string file, vector<LabelledState> &states) {
	
	// open the file
	fstream fin(file, ios::in);

	vector<string> graphs;
	string row;

	if(fin.is_open()) {
		while(fin >> row) {
			graphs.push_back(row);
		}
	} 
	else {
		cout << "Couldn't open file " << file << endl;
		return 1;
	}

	fin.close();

	// get number of qubits
	unsigned n = get_order( graphs.at(0) );
	unsigned graph_len = graphs.size();

	// outer loop variables
	unsigned N = pow(2,n);
	unsigned NN = pow(2,n*n);
	unsigned D = N*N;

	states.reserve(NN); // estimated size
	vector<binvec> B (n);
	vector<binvec> SB (n);

	// inner Lagrangian loop variables
	LabelledState state (D-1); // 0th component is ommited since it is forced to be 1 
	vector<binvec> Lagrangian_coord (N-1);
	vector<int> Lagrangian_phase (N-1);
	vector<binvec> avec (n,0);
	int phase;
	binvec aa;

	auto SP = local_symplectic_group(n);
	unsigned SP_size = SP.size();

	// graph state loop
	cout << "  Compute n-partite entangled stabiliser states for graph" << endl;

	for(unsigned g=0; g<graph_len; g++) {

		cout << "\r    #" << g+1 << " / " << graph_len << flush;
		B = graph_Lagrangian( graph6_to_adj_mat( graphs.at(g) ),n);

		// we have to take the local symplectic orbit of that Lagrangian
		for(binvec h=0; h<SP_size; h++) {

			// compute image of the Lagrangian
			for(unsigned k=0; k<n; k++) {
				SB.at(k) = matrix_vector_prod_mod2(SP.at(h).object, B.at(k));
			}

			// loop over all points a in the Lagrangian SB (except 0) and fill the vector Lagrangian_data
			// Lagrangian_coord.clear();
			// Lagrangian_phase.clear();

			for(binvec a=1; a<N; a++) {
				aa = 0;

				// get coordinates w.r.t. the canonical basis
				for(unsigned i=0; i<n; i++) {
					avec.at(i) = get_bit(a,i,n) * SB.at(i);
					aa ^= avec.at(i); 
				}

				// compute the "Lagrangian" phase phi which is i^phi with phi=0,2,4,...
				// we will write it as i^phi = (-1)^(phi/2)
				phase = phi(avec,n); 
				phase /= 2; 

				// save for later
				Lagrangian_coord.at(a-1) = aa;
				Lagrangian_phase.at(a-1) = pow(-1,phase);

			}

			// --- now, loop over generator phases and use the Lagrangian data computed before
			for(binvec s=0; s<N; s++) {
				// clear state
				state.clear();

				// note that this loop will only involve non-trivial components
				for(binvec a=1; a<N; a++) {

					// explicit stabiliser phase which is inner product of a and s 
					phase = parity(a & s); 

					// write the coefficient in the Pauli basis
					// cout << Lagrangian_coord.at(a-1) << endl;
					state.object.at( Lagrangian_coord.at(a-1) - 1 ) =  pow(-1,phase) * Lagrangian_phase.at(a-1) ;		
				}

				// add a label
				state.label = graphs.at(g)+" "+SP.at(h).label+" "+write_bits(s,n)+" c";

				// check if state already exists
				// to do that, we use binary search with std::lower_bound() which gives an iterator on the first element that is not less than pr_state
				// note that LabelledState has an overloaded < operator
				auto it = lower_bound(states.begin(), states.end(), state);

				if(it == states.end() || state < *it) {
					// element was not redundant, so add it to the list
					states.insert(it, state);
				}
				// note that procedures preserves the ordering in states ... 
			}
		}
	}

	cout << endl;

	states.shrink_to_fit();

	return 0;
}


// ---- generation of projected stabiliser states
//------------------------------------------------

// ---- LC coset generators

// representatives of the left cosets Sp(2,Z_2)/S (where S is the symplectic representation of the phase gate, S = (11,01))
vector<LabelledObject<symplectic_matrix>> S_cosets(unsigned n) {
	unsigned M = pow(3,n);

	vector<vector<binvec>> local_cosets = { vector<binvec>({0b10,0b01}), vector<binvec>({0b01,0b10}), vector<binvec>({0b01,0b11}) };
	vector<LabelledObject<symplectic_matrix>> LC_cosets (M);
	vector<vector<binvec>> Slist (n);

	vector<unsigned> trits(n, 0);
	for(binvec i=0; i<M; i++) {
		get_multi_index(n, 3, i, trits);		
		for(unsigned j=0; j<n; j++) {
			Slist.at(j) = local_cosets.at(trits.at(j));
			LC_cosets.at(i).label += to_string(trits.at(j));
		}
		LC_cosets.at(i).object = direct_sum(Slist);
	}

	return LC_cosets;
}

// representatives of the left cosets Sp(2,Z_2)/SH (where SH is the symplectic representation of the phase gate x Hadamard, SH = (11,10))
vector<LabelledObject<symplectic_matrix>> SH_cosets(unsigned n) {
	unsigned N = pow(2,n);

	vector<vector<binvec>> local_cosets = { vector<binvec>({0b10,0b01}), vector<binvec>({0b11,0b01}) };
	vector<LabelledObject<symplectic_matrix>> LC_cosets (N);
	vector<vector<binvec>> Slist (n);

	for(binvec i=0; i<N; i++) {
		for(unsigned j=0; j<n; j++) {
			Slist.at(j) = local_cosets.at( get_bit(i,j,n) );
		}
		LC_cosets.at(i).object = direct_sum(Slist);
		LC_cosets.at(i).label = write_bits(i,n);
	}

	return LC_cosets;
}


// ---- routines that rely on connectedness

// This assumes that the input are representatives of isomorphism and local complementation orbits of graphs. To get the full set of projected stabilisers, we have the following orbits of these representatives:
//	1. W.r.t. to the cosets of Sp(2,Z_2) / <S>  (where S is the symplectic representation of the phase gate, S = (11,01))
//  2. W.r.t. to the signs of the n generators
//
// The cosets are represented by {I,H,HS} and S can be effectively implemented by switching the representative diagonal entry of the adjacency matrix of the graph from 0 to 1.
int projected_states_H(const string file, vector<LabelledState> &states) {
	
	// open the file
	fstream fin(file, ios::in);

	vector<string> graphs;
	string row;

	if(fin.is_open()) {
		while(fin >> row) {
			graphs.push_back(row);
		}
	} 
	else {
		cout << "Couldn't open file " << file << endl;
		return 1;
	}

	fin.close();

	// get number of qubits
	unsigned n = get_order( graphs.at(0) );
	unsigned graph_len = graphs.size();

	// outer loop variables
	unsigned N = pow(2,n);
	unsigned M = pow(3,n);
	states.reserve(M); // estimated size
	vector<binvec> B (n);
	vector<binvec> SB (n);

	// identifies of the projected states. labels the orbits
	// labels.reserve(M);

	// inner Lagrangian loop variables
	LabelledState pr_state (n); // 0th component is ommited since it is forced to be 1 
	vector<proj_helper> Lagrangian_data;
	Lagrangian_data.reserve(N/2);
	vector<binvec> avec (n,0);
	vector<unsigned> weights (4,0);
	int phase;
	binvec aa;

	auto LC_cosets = S_cosets(n);

	// graph state loop
	cout << "  Compute projected stabiliser states for graph" << endl;

	for(unsigned g=0; g<graph_len; g++) {

		cout << "\r    #" << g+1 << " / " << graph_len << flush;
		B = graph_Lagrangian( graph6_to_adj_mat( graphs.at(g) ),n);

		// we have to take the local coset orbit of that Lagrangian
		for(binvec h=0; h<M; h++) {

			for(unsigned k=0; k<n; k++) {
				SB.at(k) = matrix_vector_prod_mod2(LC_cosets.at(h).object, B.at(k));
			}

			// loop over all points a in the Lagrangian SB (except 0) and fill the vector Lagrangian_data

			Lagrangian_data.clear();
			for(binvec a=1; a<N; a++) {
				aa = 0;

				// get coordinates w.r.t. the canonical basis
				for(unsigned i=0; i<n; i++) {
					avec.at(i) = get_bit(a,i,n) * SB.at(i);
					aa ^= avec.at(i); 
				}

				// Compute the weights. Only points with zero Z weight have to be considered
				weights = count_weights(aa,n);

				if(weights.at(2) == 0) {
					// compute Pauli component of the state corresponding to a, this is just given by a phase

					// compute the "Lagrangian" phase phi which is i^phi with phi=0,2,4,...
					// we will write it as i^phi = (-1)^(phi/2)
					phase = phi(avec,n); 
					phase /= 2; 

					// save for later
					Lagrangian_data.push_back(proj_helper(a, (weights.at(1)+weights.at(3)), (char)pow(-1,phase)));				
				}
			}

			// --- now, loop over phases and use the Lagrangian data computed before
			for(binvec s=0; s<N; s++) {
				// clear pr_state
				pr_state.clear();

				// note that this loop will only involve non-trivial components
				for(binvec i=0; i<Lagrangian_data.size(); i++) {

					// explicit stabiliser phase which is inner product of a and s 
					phase = parity(Lagrangian_data.at(i).a & s); 

					// add it to the right Fock component
					// cout << (unsigned)Lagrangian_data.at(i).fock_index << endl;
					pr_state.object.at(Lagrangian_data.at(i).fock_index - 1) += ( pow(-1,phase) * Lagrangian_data.at(i).pauli_component );		
				}

				// add a label
				pr_state.label = graphs.at(g)+" "+LC_cosets.at(h).label+" "+write_bits(s,n)+" c";

				// check if projected state pr_state already exists
				// to do that, we use binary search with std::lower_bound() which gives an iterator on the first element that is not less than pr_state
				// note that LabelledState has an overloaded < operator
				auto it = lower_bound(states.begin(), states.end(), pr_state);

				if(it == states.end() || pr_state < *it) {
					// element was not redundant, so add it to the list
					states.insert(it, pr_state);
				}
				// note that procedures preserves the ordering in states ... 
			}
		}
	}

	cout << endl;

	states.shrink_to_fit();

	return 0;
}

int projected_states_T(const string file, vector<LabelledState> &states) {
	
	// open the file
	fstream fin(file, ios::in);

	vector<string> graphs;
	string row;

	if(fin.is_open()) {
		while(fin >> row) {
			graphs.push_back(row);
		}
	} 
	else {
		cout << "Couldn't open file " << file << endl;
		return 1;
	}

	fin.close();

	// get number of qubits
	unsigned n = get_order( graphs.at(0) );
	unsigned graph_len = graphs.size();

	// outer loop variables
	unsigned N = pow(2,n);
	states.reserve(N); // estimated size
	vector<binvec> B (n);
	vector<binvec> SB (n);

	// identifies of the projected states. labels the orbits
	// labels.reserve(M);

	// inner Lagrangian loop variables
	LabelledState pr_state (n); // 0th component is ommited since it is forced to be 1 
	vector<proj_helper> Lagrangian_data;
	Lagrangian_data.reserve(N/2);
	vector<binvec> avec (n,0);
	vector<unsigned> weights (4,0);
	int phase;
	binvec aa;

	auto LC_cosets = SH_cosets(n);

	// graph state loop
	cout << "  Compute projected stabiliser states for graph" << endl;

	for(unsigned g=0; g<graph_len; g++) {

		cout << "\r    #" << g+1 << " / " << graph_len << flush;
		B = graph_Lagrangian( graph6_to_adj_mat( graphs.at(g) ),n);

		// we have to take the local coset orbit of that Lagrangian
		for(binvec h=0; h<N; h++) {

			for(unsigned k=0; k<n; k++) {
				SB.at(k) = matrix_vector_prod_mod2(LC_cosets.at(h).object, B.at(k));
			}

			// loop over all points a in the Lagrangian SB (except 0) and fill the vector Lagrangian_data

			Lagrangian_data.clear();
			for(binvec a=1; a<N; a++) {
				aa = 0;

				// get coordinates w.r.t. the canonical basis
				for(unsigned i=0; i<n; i++) {
					avec.at(i) = get_bit(a,i,n) * SB.at(i);
					aa ^= avec.at(i); 
				}

				// Compute the weights. 
				weights = count_weights(aa,n);

				// compute Pauli component of the state corresponding to a, this is just given by a phase

				// compute the "Lagrangian" phase phi which is i^phi with phi=0,2,4,...
				// we will write it as i^phi = (-1)^(phi/2)
				phase = phi(avec,n); 
				phase /= 2; 

				// save for later
				Lagrangian_data.push_back(proj_helper(a, (weights.at(1)+weights.at(2)+weights.at(3)), (char)pow(-1,phase)));				
			}

			// --- now, loop over phases and use the Lagrangian data computed before
			for(binvec s=0; s<N; s++) {
				// clear pr_state
				pr_state.clear();

				// note that this loop will only involve non-trivial components
				for(binvec i=0; i<Lagrangian_data.size(); i++) {

					// explicit stabiliser phase which is inner product of a and s 
					phase = parity(Lagrangian_data.at(i).a & s); 

					// add it to the right Fock component
					// cout << (unsigned)Lagrangian_data.at(i).fock_index << endl;
					pr_state.object.at(Lagrangian_data.at(i).fock_index - 1) += ( pow(-1,phase) * Lagrangian_data.at(i).pauli_component );		
				}

				// add a label
				pr_state.label = graphs.at(g)+" "+LC_cosets.at(h).label+" "+write_bits(s,n)+" c";

				// check if projected state pr_state already exists
				// to do that, we use binary search with std::lower_bound() which gives an iterator on the first element that is not less than pr_state
				// note that LabelledState has an overloaded < operator
				auto it = lower_bound(states.begin(), states.end(), pr_state);

				if(it == states.end() || pr_state < *it) {
					// element was not redundant, so add it to the list
					states.insert(it, pr_state);
				}
				// note that procedures preserves the ordering in states ... 
			}
		}
	}

	cout << endl;

	return 0;
}


// ---- computes the projections of product states


// the coordinates of the projection of product state \rho \otimes \sigma can be expressed by their individual projections only. If A_k(.) denotes the k-th coordinate, i.e. the k-th signed weight enumerator, then
// A_k( \rho\otimes\sigma ) = \sum_{i=0}^k A_i(\rho) A_{k-i}(\sigma).
vector<int> pr_product_state(const vector<int>& rho, const vector<int>& sigma) {
	unsigned n = rho.size();
	unsigned m = sigma.size();

	if(n == 0) {
		return sigma;
	}
	if(m == 0) {
		return rho;
	}

	vector<int> ret (n+m,0);
	vector<int> r (n+m+1,0);
	vector<int> s (n+m+1,0);

	// copy rho and sigma to r and s which are padded with zeros, to avoid headaches and bugs ;)
	copy(rho.begin(),rho.end(),r.begin()+1);
	copy(sigma.begin(),sigma.end(),s.begin()+1);
	r.at(0) = 1;
	s.at(0) = 1;

	for(unsigned k=1; k<=n+m; k++) {	
		for(unsigned i=0; i<=k; i++) {
			ret.at(k-1) += r.at(i)*s.at(k-i);
		}
	}

	return ret;
}

string pr_product_label(const string label1, const string label2) {
	if(label1.empty() || label1 == "") {
		return label2;
	}
	if(label2.empty() || label2 == "") {
		return label1;
	}

	// get the components of every label 
	vector<string> c1;
	vector<string> c2;

	istringstream ss (label1);
	string buf;

	while(ss >> buf) {
		c1.push_back(buf);
	}

	ss = istringstream(label2);
	while(ss >> buf) {
		c2.push_back(buf);
	}

	// get the graph6 representation of the product graph
	vector<binvec> A1 = graph6_to_adj_mat(c1.at(0));
	vector<binvec> A2 = graph6_to_adj_mat(c2.at(0));
	vector<binvec> A = direct_sum(A1, A2);

	string ret = adj_mat_to_graph6(A);

	// concatenate the operator and sign encoding
	ret += " "+c1.at(1)+c2.at(1);
	ret += " "+c1.at(2)+c2.at(2);
	ret += " d"; // for disconnected

	return ret;
}

LabelledState pr_product_state(const LabelledState& rho, const LabelledState& sigma) {
	unsigned n = rho.size();
	unsigned m = sigma.size();

	LabelledState ret (n+m);
	vector<int> r (n+m+1,0);
	vector<int> s (n+m+1,0);

	// copy rho and sigma to r and s which are padded with zeros, to avoid headaches and bugs ;)
	copy(rho.object.begin(),rho.object.end(),r.begin()+1);
	copy(sigma.object.begin(),sigma.object.end(),s.begin()+1);
	r.at(0) = 1;
	s.at(0) = 1;

	for(unsigned k=1; k<=n+m; k++) {	
		for(unsigned i=0; i<=k; i++) {
			ret.object.at(k-1) += r.at(i)*s.at(k-i);
		}
	}

	ret.label = pr_product_label(rho.label, sigma.label);

	return ret;
}


// filename templates are assumed to contain placeholder like %d for system size
int pr_product_states(unsigned n, vector<LabelledState> &states, string states_file_tpl, string labels_file_tpl) {

	//  get partitions of n
	auto partitions = get_partitions(n);

	// clear containers
	states.clear();

	// read vertices 
	vector<vector<LabelledState>> vertices (n-1);
	vector<unsigned> nvertices (n-1);

	for(unsigned i=0; i<n-1; i++) {
		char *fstate = new char [2*states_file_tpl.size()];
		char *flabel = new char [2*labels_file_tpl.size()];

		snprintf(fstate, 2*states_file_tpl.size(), states_file_tpl.c_str(), i+1);

		if(labels_file_tpl != "") {
			snprintf(flabel, 2*labels_file_tpl.size(), labels_file_tpl.c_str(), i+1);

			if(get_states(vertices.at(i), string(fstate), string(flabel)) != 0) {
				return 1;
			}
		}
		else {
			if(get_states(vertices.at(i), string(fstate)) != 0) {
				return 1;
			}
		}

		nvertices.at(i) = vertices.at(i).size();

		delete[] fstate;
		delete[] flabel;
	}

	LabelledState pr_state;

	for(auto p = partitions.begin()+1; p != partitions.end(); ++p) {
		
		unsigned L = p->size();
		unsigned N = 1;

		vector<unsigned> nv (L);

		// get total number of product states corresponding to that partition
		for(unsigned i=0; i<L; i++) {
			N *= nvertices.at( p->at(i)-1 );
			nv.at(i) = nvertices.at( p->at(i)-1 );
		}
		
		// build up all product states corresponding to tuples (i_1,...,i_L)
		vector<unsigned> ind (L);

		for(unsigned i=0; i<N; i++) {
			get_multi_index(nv, i, ind);

			pr_state = vertices.at( p->at(0)-1 ).at( ind.at(0) );

			for(unsigned k=1; k<L; k++) {
				pr_state = pr_product_state( pr_state, vertices.at( p->at(k)-1 ).at( ind.at(k) ) );
			}

			// check if state already exists in the list
			// to do that, we use binary search with std::lower_bound() which gives an iterator on the first element that is not less than state
			auto it = lower_bound(states.begin(), states.end(), pr_state);
			if(it == states.end() || pr_state < *it) {
				// element was not redundant, so also add it to the list
				states.insert(it, pr_state);
			}
		}
	}

	return 0;
}



// ---- 2nd level of the hierarchy

vector<int> pr_allplus (unsigned n) {
	vector<int> ret (n,0);
	for(unsigned k=1; k<=n; k++) {
		ret.at(k-1) = binomial_coeff(n,k);
	}
	return ret;
}

string pr_allplus_label (unsigned n) {
	if(n==0) {
		return "";
	}
	string ret = adj_mat_to_graph6(vector<binvec>(n,0));
	ret += " ";
	for(unsigned k=0; k<n; k++) {
		ret += "0";
	}
	ret += " ";
	for(unsigned k=0; k<n; k++) {
		ret += "0";
	}
	return ret;
}

vector<int> pr_allminus (unsigned n) {
	vector<int> ret (n,0);
	for(unsigned k=1; k<=n; k++) {
		ret.at(k-1) = binomial_coeff(n,k);
		if(k%2 != 0) {
			ret.at(k-1) *= -1;
		}
	}
	return ret;
}

string pr_allminus_label (unsigned n) {
	if(n==0) {
		return "";
	}
	string ret = adj_mat_to_graph6(vector<binvec>(n,0));
	ret += " ";
	for(unsigned k=0; k<n; k++) {
		ret += "0";
	}
	ret += " ";
	for(unsigned k=0; k<n; k++) {
		ret += "1";
	}
	return ret;
}

// ----- for building up product states for the H case

vector<int> pr_psiplus = {0, -2};
vector<int> pr_psiminus = {0, 2};
string pr_psiplus_label = "A_ 01 01";
string pr_psiminus_label = "A_ 01 11";

int bipartite_states_H(const unsigned n, vector<LabelledState> &states) {
	// get all partitions of n/2 of length 3: n/2 = k_1 + k_2 + k_3
	// then use k_1 psi+, k_2 psi- and 2*k_3 (+1) +/- states
	
	for(unsigned k1=0; k1<=n/2; k1++) {
		for(unsigned k2=0; k2<=n/2-k1; k2++) {
			unsigned k3 = n/2-k1-k2;

			vector<int> state;
			string label;

			for(unsigned i=0; i<k1; i++) {
				state = pr_product_state(pr_psiplus, state);
				label = pr_product_label(pr_psiplus_label, label);
			}
			for(unsigned i=0; i<k2; i++) {
				state = pr_product_state(pr_psiminus, state);
				label = pr_product_label(pr_psiminus_label, label);
			}

			vector<int> statem = state;
			string labelm = label;

			if(2*k3+n%2 != 0) {
				state = pr_product_state(pr_allplus(2*k3+n%2), state);
				label = pr_product_label(pr_allplus_label(2*k3+n%2), label);

				statem = pr_product_state(pr_allminus(2*k3+n%2), statem);
				labelm = pr_product_label(pr_allminus_label(2*k3+n%2), labelm);

				states.emplace_back(statem, labelm);
			}

			// append
			states.emplace_back(state, label);		
		}
	}

	return 0;
}

// ----- for building up product states for the T case
vector<int> pr_gammaminus = {0, -3};
vector<int> pr_gammaplus = {0, 3};
string pr_gammaminus_label = "A_ 01 11 c";
string pr_gammaplus_label = "A_ 00 00 c";

int bipartite_states_T(const unsigned n, vector<LabelledState> &states) {
	// get all partitions of n/2 of length 3: n/2 = k_1 + k_2 + k_3
	// then use k_1 psi+, k_2 psi- and 2*k_3 (+1) +/- states
	
	for(unsigned k1=0; k1<=n/2; k1++) {
		for(unsigned k2=0; k2<=n/2-k1; k2++) {
			unsigned k3 = n/2-k1-k2;

			vector<int> state;
			string label;

			for(unsigned i=0; i<k1; i++) {
				state = pr_product_state(pr_gammaplus, state);
				label = pr_product_label(pr_gammaplus_label, label);
			}
			for(unsigned i=0; i<k2; i++) {
				state = pr_product_state(pr_gammaminus, state);
				label = pr_product_label(pr_gammaminus_label, label);
			}

			vector<int> statem = state;
			string labelm = label;

			if(2*k3+n%2 != 0) {
				state = pr_product_state(pr_allplus(2*k3+n%2), state);
				label = pr_product_label(pr_allplus_label(2*k3+n%2), label);

				statem = pr_product_state(pr_allminus(2*k3+n%2), statem);
				labelm = pr_product_label(pr_allminus_label(2*k3+n%2), labelm);

				states.emplace_back(statem, labelm);
			}

			// append
			states.emplace_back(state, label);		
		}
	}
	return 0;
}



// ---- I/O of the generated states
// ---------------------------------

int write_states(const vector<LabelledState> &states, string states_file, string labels_file) {
	// write the states
	fstream fout(states_file, ios::out);
	unsigned nstates = states.size();

	if(fout.is_open()) {
		for(unsigned i=0; i<nstates; i++) {
			for(auto c : states.at(i).object) {
				fout << c << " ";
			}
			fout << endl;
		}	
		fout.close();
	}
	else {
		cout << "Error in write_states : Couldn't open file " + states_file + " for writing" << endl;
		return 1;
	}

	if(labels_file != "") {
		fout.open(labels_file, ios::out);
		if(fout.is_open()) {
			for(auto s : states) {
				fout << s.label << endl;
			}	
		}
		else {
			cout << "Error in write_labels : Couldn't open file " + labels_file + " for writing" << endl;
			return 2;
		}
	}

	return 0;
}

int write_con_states(const vector<LabelledState> &states, string states_file, string labels_file) {
	// write the states
	fstream fout(states_file, ios::out);
	fstream fout2(labels_file, ios::out);
	unsigned nstates = states.size();

	if(fout.is_open() && fout2.is_open()) {
		for(unsigned i=0; i<nstates; i++) {
			if(states.at(i).label.back() == 'c') {
				fout2 << states.at(i).label << endl;

				for(auto c : states.at(i).object) {
					fout << c << " ";
				}
				fout << endl;
			}
		}	
		fout.close();
		fout2.close();
	}
	else {
		cout << "Error in write_states : Couldn't open file " + states_file + " for writing" << endl;
		return 1;
	}

	return 0;
}

int get_states(vector<LabelledState> &states, string states_file, string labels_file) {
	unsigned nstates = get_number_of_lines(states_file);
	
	fstream fin(states_file, ios::in);
	string line;

	if(fin.is_open()) {
		states.resize(nstates);
		unsigned i = 0;
		while(getline(fin,line)) {
			istringstream sl (line);
			string buf;

			while(sl >> buf) {
				states.at(i).object.push_back(stoi(buf));
			}

			++i;
		}
		fin.close();
	}
	else {
		cout << "Error in get_states : Couldn't open states file " + states_file + " for reading" << endl;
		return 1;
	}

	if(labels_file != "") {

		fin.open(labels_file, ios::in);

		if(fin.is_open()) {
			string buf;
			unsigned i = 0;
			while(getline(fin,buf) && i < nstates) {
				states.at(i).label = buf;
				++i;
			}
		}
		else {
			cout << "Error in get_labels : Couldn't open labels file " + labels_file + " for reading" << endl;
			return 2;
		}	
	}

	return 0;
}

int write_state_vectors(const vector<LabelledStateVector> &states, string states_file, string labels_file) {
	// write the states
	fstream fout(states_file, ios::out);
	unsigned nstates = states.size();

	fout.precision(16);

	if(fout.is_open()) {
		for(unsigned i=0; i<nstates; i++) {
			for(auto c : states.at(i).object) {
				fout << c << " ";
			}
			fout << endl;
		}	
		fout.close();
	}
	else {
		cout << "Error in write_states : Couldn't open file " + states_file + " for writing" << endl;
		return 1;
	}

	if(labels_file != "") {
		fout.open(labels_file, ios::out);
		if(fout.is_open()) {
			for(auto s : states) {
				fout << s.label << endl;
			}	
		}
		else {
			cout << "Error in write_labels : Couldn't open file " + labels_file + " for writing" << endl;
			return 2;
		}
	}

	return 0;
}