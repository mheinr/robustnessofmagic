#include <iostream>
#include <fstream>
#include <tclap/CmdLine.h>
#include <algorithm>
#include <chrono>

#ifndef STABILISER_H
#include "stabiliser.h"
#endif

#ifndef GLPKCONVEXSEPARATION_H
#include "GLPKConvexSeparation.h"
#endif

using namespace std;


/* Program to generate a sub-polytope of the projected stabiliser polytope which yields an approximate optimisation problem for RoM.

	The sub-polytope is spanned by product states made out of Bell-type factors and X eigenstates.

	Input to the program:
		(*) number of qubits 

*/



int main(int argc, char** argv) {

// ----------------------------------
// ----- parse comand-line parameters
// ----------------------------------

string outfile,state;
unsigned n;
bool elim,verbose;

try {

	TCLAP::CmdLine cmd("Generate the vertices of a sub-polytope of the projected stabiliser polytope spanned by projections of at most bipartite states.", ' ', "1.0");

	// arguments
	TCLAP::ValueArg<string> output_arg ("o", "outfile", "Output file name that will be used for writing the reduced constraint matrix", true, "out", "string");
	cmd.add(output_arg);

	TCLAP::ValueArg<unsigned> n_arg ("q", "qubits", "Number of qubits", true, 1, "Unsigned integer");
	cmd.add(n_arg);

	TCLAP::SwitchArg elim_arg ("e", "eliminate", "Flag which enables elimination of redundant points from the set, i.e. those points which are convex combinations of the others.", false);
	cmd.add(elim_arg);

	TCLAP::SwitchArg verb_arg ("v", "verbose", "Does what it promises.", false);
	cmd.add(verb_arg);

	vector<string> allowed_states = { "T", "H" };
	TCLAP::ValuesConstraint<string> state_con ( allowed_states );
	TCLAP::ValueArg<string> state_arg ("s", "state", "Which state to use: Either H or T", true, "H", &state_con);
	cmd.add(state_arg);

	cmd.parse(argc, argv);

	outfile = output_arg.getValue();
	n = n_arg.getValue();
	elim = elim_arg.getValue();
	state = state_arg.getValue();

} catch (TCLAP::ArgException &e) { 
	cerr << "Error: " << e.error() << " for arg " << e.argId() << endl; 
}


// ----------------------------------
// ------ generation of states
// ----------------------------------


// these will hold the states
vector<LabelledState> states;

cout << "----------------------------------------------------------------------" << endl;
cout << "Generate projected stabiliser sub-polytope for n = " << n << endl;
cout << "----------------------------------------------------------------------" << endl;


// write program call to stdout
cout << endl;
cout << "# Program call" << endl;
cout << "#-------------" << endl;
cout << "   ";
for(unsigned i=0; i<argc; i++) {
	cout << argv[i] << " ";
}
cout << endl << endl;


// timing
auto t1 = chrono::high_resolution_clock::now();

if(state == "H") {
	bipartite_states_H(n, states);
}
else {
	bipartite_states_T(n, states);
}

// timing
auto t2 = chrono::high_resolution_clock::now();
chrono::duration<double, milli> fp_ms = t2 - t1;

cout << "   Found " << states.size() << " bipartite entangled states." << endl;
cout << "   Generation took " <<  fp_ms.count() << " ms." << endl;



// ----------------------------------
// ----- final elimination round
// ----------------------------------

GLPKConvexSeparation lp (states);
if(verbose == false) {
	lp.set_verbosity(1);
}
else {
	lp.set_verbosity(2);
}

if(elim == true) {

	cout << endl;
	cout << "-------------------------------------------------" << endl;
	cout << "Eliminate redundant points from the total set" << endl;
	cout << "-------------------------------------------------" << endl;
	cout << endl;

	lp.print_parameters();
	int dels = lp.delete_redundant_points();
	cout << "   Deleted " << dels << " points." << endl;
	lp.print_parameters();

 	states = lp.iget_labelled_vertices();
}

// sort the list
sort(states.begin(), states.end());

// timing
auto t5 = chrono::high_resolution_clock::now();
chrono::duration<double, milli> fp_ms3 = t5 - t1;

cout << "Total runtime: " <<  fp_ms3.count() << " ms." << endl;


// ----------------------------------
// ----- write output
// ----------------------------------

write_states(states, outfile+to_string(n)+".mat", outfile+to_string(n)+".lab");

}