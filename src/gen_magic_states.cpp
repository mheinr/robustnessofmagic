#include <iostream>
#include <fstream>
#include <streambuf>
#include <tclap/CmdLine.h>
#include <algorithm>
#include <chrono>


#ifndef STABILISER_H
#include "stabiliser.h"
#endif


using namespace std;


int main(int argc, char** argv) {

// ----------------------------------
// ----- parse comand-line parameters
// ----------------------------------

string outfile;
bool verbose;
unsigned n;

try {

	TCLAP::CmdLine cmd("Generate n-qubit magic states.", ' ', "1.0");

	// arguments
	TCLAP::ValueArg<unsigned> n_arg ("n", "nqubits", "Number of qubits", true, 1, "Nonnegative integer");
	cmd.add(n_arg);

	TCLAP::ValueArg<string> output_arg ("o", "outfile", "Output file name that will be used to write the stabiliser state coefficients in the Pauli basis", true, "out", "string");
	cmd.add(output_arg);

	TCLAP::SwitchArg verb_arg ("v", "verbose", "Does what it promises.", false);
	cmd.add(verb_arg);



	cmd.parse(argc, argv);

	outfile = output_arg.getValue();
	verbose = verb_arg.getValue();
	n = n_arg.getValue();

} catch (TCLAP::ArgException &e) { 
	cerr << "Error: " << e.error() << " for arg " << e.argId() << endl; 
	return 1;
}


// ----------------------------------
// ------ generation of states
// ----------------------------------


cout << "#--------------------------------------------" << endl;
cout << "# Generation of magic states for n = " << n << " qubits" << endl;
cout << "#--------------------------------------------" << endl;


// write program call to stdout
cout << endl;
cout << "# Program call" << endl;
cout << "#-------------" << endl;
cout << "   ";
for(unsigned i=0; i<argc; i++) {
	cout << argv[i] << " ";
}
cout << endl << endl;


// timing
auto t1 = chrono::high_resolution_clock::now();

vector<LabelledStateVector> states = gen_magic_states(n);

auto t2 = chrono::high_resolution_clock::now();
chrono::duration<double, milli> fp_ms = t2 - t1;

cout << "   Found " << states.size()  << " magic states." << endl;
cout << "   Generation took " <<  fp_ms.count() << " ms." << endl;

// write to file
write_state_vectors(states, outfile+"_"+to_string(n)+".mat", outfile+"_"+to_string(n)+".lab");

}