#include <iostream>
#include <fstream>
#include <streambuf>
#include <tclap/CmdLine.h>
#include <algorithm>
#include <chrono>


#ifndef STABILISER_H
#include "stabiliser.h"
#endif


using namespace std;


int main(int argc, char** argv) {

// ----------------------------------
// ----- parse comand-line parameters
// ----------------------------------

string outfile;
bool verbose,fs;
unsigned n;

try {

	TCLAP::CmdLine cmd("Generate the vertices of the stabiliser polytope.", ' ', "1.0");

	// arguments
	TCLAP::ValueArg<unsigned> n_arg ("n", "nqubits", "Number of qubits", true, 1, "Nonnegative integer");
	cmd.add(n_arg);

	TCLAP::ValueArg<string> output_arg ("o", "outfile", "Output file name that will be used to write the stabiliser state coefficients in the Pauli basis", true, "out", "string");
	cmd.add(output_arg);

	TCLAP::SwitchArg verb_arg ("v", "verbose", "Does what it promises.", false);
	cmd.add(verb_arg);

	TCLAP::SwitchArg fs_arg ("s", "fullsupport", "Generates only full support stabiliser states.", false);
	cmd.add(fs_arg);


	cmd.parse(argc, argv);

	outfile = output_arg.getValue();
	verbose = verb_arg.getValue();
	n = n_arg.getValue();
	fs = fs_arg.getValue();

} catch (TCLAP::ArgException &e) { 
	cerr << "Error: " << e.error() << " for arg " << e.argId() << endl; 
	return 1;
}


// ----------------------------------
// ------ generation of states
// ----------------------------------



cout << "#--------------------------------------------" << endl;
cout << "# Generation of stabiliser states" << endl;
cout << "# from graph representatives for n = " << n << " qubits" << endl;
cout << "#--------------------------------------------" << endl;


// write program call to stdout
cout << endl;
cout << "# Program call" << endl;
cout << "#-------------" << endl;
cout << "   ";
for(unsigned i=0; i<argc; i++) {
	cout << argv[i] << " ";
}
cout << endl << endl;


// these will hold connected and product states
vector<LabelledState> states;


// timing
auto t1 = chrono::high_resolution_clock::now();

// ---- generate orbits of connected graphs
// ----------------------------------------


cout << endl;
cout << "# Generate LC orbit of graph states" << endl;
cout << "#----------------------------------" << endl;
cout << endl;

if(fs == true) {
	cout << "   ## Full support stabiliser states only ##" << endl << endl;

	if(fs_stabiliser_states(n, states) != 0) {
		cout << "Error in generating stabiliser states." << endl;
		cout << "Will now hold." << endl;
		return 1;
	}
}
else {
	if(stabiliser_states(n, states) != 0) {
		cout << "Error in generating stabiliser states." << endl;
		cout << "Will now hold." << endl;
		return 1;
	}
}



// timing
auto t2 = chrono::high_resolution_clock::now();
chrono::duration<double, milli> fp_ms = t2 - t1;

cout << "   Found " << states.size() << " stabiliser states." << endl;
cout << "   Generation took " <<  fp_ms.count() << " ms." << endl;

write_states(states, outfile+to_string(n)+".mat", outfile+to_string(n)+".lab");


// generate state vectors.
cout << endl;
cout << "   Will now convert states to Hilbert space vectors ... " << endl;

auto t3 = chrono::high_resolution_clock::now();

vector<LabelledStateVector> state_vectors;
if(fs == true) {
	// transform(states.begin(), states.end(), back_inserter(state_vectors), find_fs_state_vector);
	state_vectors = fs_stabiliser_state_vectors(n);
}
else {
	// for(unsigned k=0; k<10; k++) {
	// 	cout << "state #" << k << endl;
	// 	state_vectors.push_back(find_state_vector(states.at(k)));
	// 	cout << endl;
	// }
	transform(states.begin(), states.end(), back_inserter(state_vectors), find_state_vector);
}

auto t4 = chrono::high_resolution_clock::now();
chrono::duration<double, milli> fp_ms2 = t4 - t3;

cout << "   Found " << state_vectors.size()  << " stabiliser state vectors." << endl;
cout << "   Generation took " <<  fp_ms2.count() << " ms." << endl;

// write to file
write_state_vectors(state_vectors, outfile+"vectors_"+to_string(n)+".mat", outfile+"vectors_"+to_string(n)+".lab");

}