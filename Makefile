SRCDIR=src
OBJDIR=obj
INCDIR=inc
BINDIR=bin

CC=gcc
CXX=g++
RM=rm -f
CPPFLAGS=-g -O3 -I$(INCDIR)
LDFLAGS=
LDLIBS=-lglpk -lm


all: proj_stabilisers robustness hierarchy_vertices gen_stabilisers gen_magic_states

proj_stabilisers: $(OBJDIR)/proj_stabilisers.o $(OBJDIR)/stabiliser.o $(OBJDIR)/utilities.o $(OBJDIR)/symplectic.o $(OBJDIR)/GLPKFormat.o $(OBJDIR)/GLPKConvexSeparation.o
	$(CXX) -o $(BINDIR)/$@ $^ $(CPPFLAGS) $(LDFLAGS) $(LDLIBS)

robustness: $(OBJDIR)/robustness.o $(OBJDIR)/utilities.o $(OBJDIR)/stabiliser.o $(OBJDIR)/symplectic.o $(OBJDIR)/GLPKFormat.o $(OBJDIR)/GLPKL1Minimisation.o
	$(CXX) -o $(BINDIR)/$@ $^ $(CPPFLAGS) $(LDFLAGS) $(LDLIBS)

hierarchy_vertices: $(OBJDIR)/hierarchy_vertices.o $(OBJDIR)/stabiliser.o $(OBJDIR)/utilities.o $(OBJDIR)/symplectic.o $(OBJDIR)/GLPKFormat.o $(OBJDIR)/GLPKConvexSeparation.o
	$(CXX) -o $(BINDIR)/$@ $^ $(CPPFLAGS) $(LDFLAGS) $(LDLIBS)

gen_stabilisers: $(OBJDIR)/gen_stabilisers.o $(OBJDIR)/stabiliser.o $(OBJDIR)/utilities.o $(OBJDIR)/symplectic.o
	$(CXX) -o $(BINDIR)/$@ $^ $(CPPFLAGS) $(LDFLAGS) $(LDLIBS)

gen_magic_states: $(OBJDIR)/gen_magic_states.o $(OBJDIR)/stabiliser.o $(OBJDIR)/utilities.o $(OBJDIR)/symplectic.o
	$(CXX) -o $(BINDIR)/$@ $^ $(CPPFLAGS) $(LDFLAGS) $(LDLIBS)


$(OBJDIR)/proj_stabilisers.o: $(SRCDIR)/proj_stabilisers.cpp $(INCDIR)/stabiliser.h $(INCDIR)/GLPKConvexSeparation.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/robustness.o: $(SRCDIR)/robustness.cpp $(INCDIR)/stabiliser.h $(INCDIR)/GLPKL1Minimisation.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/hierarchy_vertices.o: $(SRCDIR)/hierarchy_vertices.cpp $(INCDIR)/stabiliser.h $(INCDIR)/GLPKConvexSeparation.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/gen_stabilisers.o: $(SRCDIR)/gen_stabilisers.cpp $(INCDIR)/stabiliser.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/gen_magic_states.o: $(SRCDIR)/gen_magic_states.cpp $(INCDIR)/stabiliser.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/stabiliser.o: $(SRCDIR)/stabiliser.cpp $(INCDIR)/stabiliser.h $(INCDIR)/symplectic.h $(INCDIR)/utilities.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/utilities.o: $(SRCDIR)/utilities.cpp $(INCDIR)/utilities.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/symplectic.o: $(SRCDIR)/symplectic.cpp $(INCDIR)/symplectic.h $(INCDIR)/utilities.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/GLPKFormat.o:  $(SRCDIR)/GLPKFormat.cpp $(INCDIR)/GLPKFormat.h $(INCDIR)/utilities.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/GLPKConvexSeparation.o: $(SRCDIR)/GLPKConvexSeparation.cpp $(INCDIR)/GLPKConvexSeparation.h $(INCDIR)/ConvexSeparation.h $(INCDIR)/utilities.h $(INCDIR)/PointGenerator.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)

$(OBJDIR)/GLPKL1Minimisation.o: $(SRCDIR)/GLPKL1Minimisation.cpp $(INCDIR)/GLPKL1Minimisation.h $(INCDIR)/L1Minimisation.h $(INCDIR)/GLPKFormat.h $(INCDIR)/utilities.h
	$(CXX) -c -o $@ $< $(CPPFLAGS)




clean:
	$(RM) $(OBJDIR)/*

distclean: clean
	$(RM) $(BINDIR)/*